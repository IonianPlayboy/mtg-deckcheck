/* eslint-disable @typescript-eslint/no-unused-vars */
export default {
	mode: "spa",
	/*
	 ** Headers of the page
	 */
	head: {
		title: "Deckcheck",
		meta: [
			{ charset: "utf-8" },
			{
				name: "viewport",
				content: "width=device-width, initial-scale=1"
			},
			{
				hid: "description",
				name: "description",
				content: "A MTG Web App to check your deck"
			}
		],
		link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
	},
	/*
	 ** Customize the progress-bar color
	 */
	loading: { color: "#fff" },
	/*
	 ** Global CSS
	 */
	css: [],
	/*
	 ** Plugins to load before mounting the App
	 */
	plugins: ["@/plugins/vue-composition-api.ts"],
	/*
	 ** Nuxt.js modules
	 */
	modules: [
		// Doc: https://axios.nuxtjs.org/usage
		"@nuxtjs/axios",
		"@nuxtjs/pwa",
		"@nuxtjs/eslint-module"
	],
	pwa: {
		workbox: {
			runtimeCaching: [
				{
					// Should be a regex string. Compiles into new RegExp('https://my-cdn.com/.*')
					urlPattern: "https://api.scryfall.com/.*"
					// Defaults to `networkFirst` if omitted
					// handler: 'networkFirst',
					// Defaults to `GET` if omitted
					// method: 'GET'
				}
			]
		}
	},
	/*
	 ** Axios module configuration
	 ** See https://axios.nuxtjs.org/options
	 */
	axios: {},
	/*
	 ** Build configuration
	 */
	build: {
		// babelrc: true
		babel: {
			// envName: server, client, modern
			// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
			presets({ envName }) {
				return [
					[
						"@nuxt/babel-preset-app",
						{
							corejs: { version: 3 }
							// ignore: [/[/\\]core-js/, /@babel[/\\]runtime/]
						}
					]
				];
			}
		}
		/*
		 ** You can extend webpack config here
		 */
		// extend(config, ctx) {}
	},
	generate: {
		subFolders: false
	},
	buildModules: ["@nuxt/typescript-build"],
	typescript: {
		typeCheck: {
			eslint: true
		},
		ignoreNotFoundWarnings: true
	}
};
