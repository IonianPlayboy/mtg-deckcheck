/**
 * @jest-environment node
 */

import { reactive, computed } from "@vue/composition-api";
import "@/toolBox/tsDefinitions/DecklistDefinitions";

const store = reactive({
	currentDecklist: "",
	decklistCollection: {} as DeckCollection
});

const generateDecklistKey = (deckList: DeckList): string => {
	const cardList = Object.values(deckList).flatMap(card =>
		Object.values(card)
	) as Array<Card>;
	const decklistKey = cardList.reduce((result, card, index) => {
		const separator = index !== cardList.length - 1 ? "_" : "";
		result += `${card.tcgplayer_id}-${card.quantity}${separator}`;
		return result;
	}, "") as string;
	return decklistKey;
};
const setDeckAsCurrent = (decklistKey: string): void => {
	store.currentDecklist = decklistKey;
};

const addDeckToCollection = (deckToAdd: DeckList, decklistKey: string): void => {
	store.decklistCollection[decklistKey] = deckToAdd;
};

const submitNewDeck = (newDeck: DeckList): void => {
	const decklistKey = generateDecklistKey(newDeck);
	if (store.decklistCollection[decklistKey] !== undefined) return;
	addDeckToCollection(newDeck, decklistKey);
	setDeckAsCurrent(decklistKey);
};

const currentDeck = computed(() => store.decklistCollection[store.currentDecklist]);

const useCurrentDeck = (): typeof currentDeck => {
	return currentDeck;
};

export { store, submitNewDeck, useCurrentDeck };
