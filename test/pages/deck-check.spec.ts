import "@/plugins/vue-composition-api.ts";
import "@/toolBox/tsDefinitions/DecklistDefinitions";
import { mocked } from "ts-jest/utils";
import { shallowMount, RouterLinkStub } from "@vue/test-utils";
import { useCurrentDeck } from "@/storage/decklistStore";
import deckCheck from "@/pages/deck-check.vue";

interface Ref<T> {
	value: T;
}

jest.mock("@/storage/decklistStore");
const mockedUseCurrDeck = mocked(useCurrentDeck);
beforeEach(() => {
	mockedUseCurrDeck.mockImplementation(
		(): Ref<DeckList> =>
			(({
				value: {
					mainboard: {
						"Liliana, Dreadhorde General": {
							name: "Liliana, Dreadhorde General",
							// eslint-disable-next-line @typescript-eslint/camelcase
							tcgplayer_id: 187117,
							quantity: 2
						},
						"Judith, the Scourge Diva": {
							name: "Judith, the Scourge Diva",
							// eslint-disable-next-line @typescript-eslint/camelcase
							tcgplayer_id: 182949,
							quantity: 3
						}
					},
					sideboard: {
						Duress: {
							name: "Duress",
							// eslint-disable-next-line @typescript-eslint/camelcase
							tcgplayer_id: 192877,
							quantity: 4
						}
					}
				},
				mainboard: {
					"Liliana, Dreadhorde General": {
						name: "Liliana, Dreadhorde General",
						// eslint-disable-next-line @typescript-eslint/camelcase
						tcgplayer_id: 187117,
						quantity: 2
					},
					"Judith, the Scourge Diva": {
						name: "Judith, the Scourge Diva",
						// eslint-disable-next-line @typescript-eslint/camelcase
						tcgplayer_id: 182949,
						quantity: 3
					}
				},
				sideboard: {
					Duress: {
						name: "Duress",
						// eslint-disable-next-line @typescript-eslint/camelcase
						tcgplayer_id: 192877,
						quantity: 4
					}
				}
			} as unknown) as Ref<DeckList>)
	);
	mockedUseCurrDeck.mockClear();
});
describe("deck-check", () => {
	it("is a Vue instance", () => {
		const wrapper = shallowMount(deckCheck, {
			stubs: {
				NuxtLink: RouterLinkStub
			}
		});
		expect(wrapper.isVueInstance()).toBeTruthy();
	});
	it("renders correctly", () => {
		const wrapper = shallowMount(deckCheck, {
			stubs: {
				NuxtLink: RouterLinkStub
			}
		});
		expect(wrapper.element).toMatchSnapshot();
	});
	it("shows nothing if there is no current deck", () => {
		mockedUseCurrDeck.mockImplementation(
			() => ({ value: (undefined as unknown) as DeckList } as Ref<DeckList>)
		);
		const wrapper = shallowMount(deckCheck, {
			stubs: {
				NuxtLink: RouterLinkStub
			}
		});
		expect(wrapper.contains(".board")).toBe(false);
		expect(wrapper.contains(".noDeck")).toBe(true);
	});
	it("shows only the mainboard if there is no sideboard", () => {
		mockedUseCurrDeck.mockImplementation(
			() =>
				(({
					value: {
						mainboard: {
							"Liliana, Dreadhorde General": {
								name: "Liliana, Dreadhorde General",
								// eslint-disable-next-line @typescript-eslint/camelcase
								tcgplayer_id: 187117,
								quantity: 2
							},
							"Judith, the Scourge Diva": {
								name: "Judith, the Scourge Diva",
								// eslint-disable-next-line @typescript-eslint/camelcase
								tcgplayer_id: 182949,
								quantity: 3
							}
						},
						sideboard: {}
					},
					mainboard: {
						"Liliana, Dreadhorde General": {
							name: "Liliana, Dreadhorde General",
							// eslint-disable-next-line @typescript-eslint/camelcase
							tcgplayer_id: 187117,
							quantity: 2
						},
						"Judith, the Scourge Diva": {
							name: "Judith, the Scourge Diva",
							// eslint-disable-next-line @typescript-eslint/camelcase
							tcgplayer_id: 182949,
							quantity: 3
						}
					},
					sideboard: {}
				} as unknown) as Ref<DeckList>)
		);
		const wrapper = shallowMount(deckCheck, {
			stubs: {
				NuxtLink: RouterLinkStub
			}
		});
		expect(wrapper.contains(".main")).toBe(true);
		expect(wrapper.contains(".side")).toBe(false);
		expect(wrapper.contains(".noDeck")).toBe(false);
	});
});
