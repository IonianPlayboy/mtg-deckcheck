import "@/plugins/vue-composition-api.ts";
import { mocked } from "ts-jest/utils";
import { shallowMount } from "@vue/test-utils";
import * as getCardData from "@/toolBox/apiMethods";
import index from "@/pages/index.vue";

const router = {
	push: jest.fn()
};

jest.mock("@/toolBox/apiMethods");
const mockedGetCard = mocked(getCardData.default);
beforeEach(() => {
	mockedGetCard.mockImplementation((request: string) => {
		const cardName = request;
		const card = { name: cardName } as Card;
		return Promise.resolve(card);
	});
	mockedGetCard.mockClear();
	router.push.mockClear();
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const flushPromises = (): Promise<any> => new Promise(resolve => resolve(setImmediate));

describe("index", () => {
	it("is a Vue instance", () => {
		const wrapper = shallowMount(index, {
			mocks: {
				$router: router
			}
		});
		expect(wrapper.isVueInstance()).toBeTruthy();
	});
	it("renders correctly", () => {
		const wrapper = shallowMount(index, {
			mocks: {
				$router: router
			}
		});
		expect(wrapper.element).toMatchSnapshot();
	});
	it("shows nothing user submits an empty deck", () => {
		const wrapper = shallowMount(index, {
			mocks: {
				$router: router
			}
		});
		wrapper.find("button").trigger("click");
		expect(wrapper.contains(".loading")).toBe(false);
		expect(wrapper.contains(".invalid")).toBe(false);
		expect(wrapper.contains(".error")).toBe(false);
	});
	it("shows an error message when the user submits a deck with a non-valid card", async () => {
		const wrapper = shallowMount(index, {
			mocks: {
				$router: router
			}
		});
		wrapper.find("textarea").setValue("2 Liliana, Dreadhorde General\noui");
		wrapper.find("button").trigger("click");
		await flushPromises();
		expect(wrapper.contains(".invalid")).toBe(true);
		expect(wrapper.find(".invalid").text()).toContain("oui");
	});
	it("shows a loading indicator when user submits a non-empty and valid deck", async () => {
		const wrapper = shallowMount(index, {
			mocks: {
				$router: router
			}
		});
		wrapper.find("textarea").setValue("2 Liliana, Dreadhorde General");
		wrapper.find("button").trigger("click");
		await flushPromises();
		expect(wrapper.contains(".loading")).toBe(true);
	});

	it("shows an error message when a card lookup fail", async () => {
		mockedGetCard.mockImplementation((request: string) => {
			const resp = {
				response: {
					status: 404,
					statusText: "Not Found"
				},
				config: {
					url: `https://api.scryfall.com/cards/search?q=${request}`
				}
			};
			return Promise.reject(resp);
		});
		const wrapper = shallowMount(index, {
			mocks: {
				$router: router
			}
		});
		wrapper
			.find("textarea")
			.setValue(
				"2 Liliana, Dreadhorde General\n3 Judith, the Scourge Diva\n\n4 Duress"
			);
		wrapper.find("button").trigger("click");
		await flushPromises();
		setTimeout(() => {
			expect(mockedGetCard).toHaveBeenCalled();
			expect(wrapper.contains(".error")).toBe(true);
			expect(wrapper.find(".error").text()).toContain(
				"Liliana, Dreadhorde General"
			);
		}, 0);
	});
	it("looks up cards data when user submits a valid deck", async () => {
		const wrapper = shallowMount(index, {
			mocks: {
				$router: router
			}
		});
		wrapper
			.find("textarea")
			.setValue(
				"2 Liliana, Dreadhorde General\n3 Judith, the Scourge Diva\n\n4 Duress"
			);
		wrapper.find("button").trigger("click");
		await flushPromises();
		setTimeout(() => {
			expect(mockedGetCard).toHaveBeenCalledTimes(3);
			expect(mockedGetCard).toHaveBeenNthCalledWith(
				1,
				expect.stringContaining("Liliana, Dreadhorde General")
			);
			expect(mockedGetCard).toHaveBeenNthCalledWith(
				2,
				expect.stringContaining("Judith, the Scourge Diva")
			);
			expect(mockedGetCard).toHaveBeenLastCalledWith(
				expect.stringContaining("Duress")
			);
		}, 0);
	});
	it("goes to the deckcheck page once the deck is loaded", async () => {
		const wrapper = shallowMount(index, {
			mocks: {
				$router: router
			}
		});
		wrapper
			.find("textarea")
			.setValue(
				"2 Liliana, Dreadhorde General\n3 Judith, the Scourge Diva\n\n4 Duress"
			);
		wrapper.find("button").trigger("click");
		await flushPromises();
		setTimeout(() => {
			expect(router.push).toHaveBeenCalledTimes(1);
			expect(router.push).toHaveBeenCalledWith("/deck-check");
		}, 0);
	});
});
