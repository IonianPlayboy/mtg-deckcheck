import "@/plugins/vue-composition-api.ts";
import { store, submitNewDeck } from "@/storage/decklistStore";

const deckToAdd = {
	mainboard: {
		"Liliana, Dreadhorde General": {
			name: "Liliana, Dreadhorde General",
			// eslint-disable-next-line @typescript-eslint/camelcase
			tcgplayer_id: 187117,
			quantity: 2
		},
		"Judith, the Scourge Diva": {
			name: "Judith, the Scourge Diva",
			// eslint-disable-next-line @typescript-eslint/camelcase
			tcgplayer_id: 182949,
			quantity: 3
		}
	},
	sideboard: {
		Duress: {
			name: "Duress",
			// eslint-disable-next-line @typescript-eslint/camelcase
			tcgplayer_id: 192877,
			quantity: 4
		}
	}
};

describe("store", () => {
	it("initialises properly", () => {
		expect(store).toMatchObject({
			currentDecklist: "",
			decklistCollection: {}
		});
	});
	it("add a new deck to the collection correctly", () => {
		submitNewDeck(deckToAdd);
		expect(Object.entries(store.decklistCollection).length).toBe(1);
		expect(Object.keys(store.decklistCollection)[0]).toBe(
			"187117-2_182949-3_192877-4"
		);
	});
	it("does not add a single deck multiple times to the collection", () => {
		submitNewDeck(deckToAdd);
		submitNewDeck(deckToAdd);
		expect(Object.entries(store.decklistCollection).length).toBe(1);
	});
	it("set the most recently added deck as the current deck", () => {
		submitNewDeck(deckToAdd);
		expect(store.currentDecklist).toBe("187117-2_182949-3_192877-4");
	});
});
