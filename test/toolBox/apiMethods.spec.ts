import "@/plugins/vue-composition-api.ts";
import { mocked } from "ts-jest/utils";
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
import ScryfallQueueHandler, { mockedGetCard } from "@/toolBox/queueHandler";
import getCardData from "@/toolBox/apiMethods";
import "@/toolBox/tsDefinitions/DecklistDefinitions";

// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
global.fetch = jest.fn();
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
const mockedFetch = mocked(global.fetch);
jest.mock("@/toolBox/queueHandler");
const mockedQueueHandler = mocked(ScryfallQueueHandler);

beforeEach(() => {
	mockedFetch.mockImplementation((url: string) => {
		const nameInDatabase = [
			"Liliana, Dreadhorde General",
			"Judith, the Scourge Diva",
			"Duress"
		];
		const cardName = url.replace("https://deckcheck-api.netlify.com/search/", "");
		return new Promise((resolve, reject) => {
			if (nameInDatabase.find(el => el === cardName))
				resolve({
					ok: true,
					statusCode: 200,
					json: () =>
						({
							name: cardName,
							// eslint-disable-next-line @typescript-eslint/camelcase
							tcgplayer_id: 187117,
							quantity: 2
						} as Card)
				});
			else {
				reject(
					new Error("Could not find the card you were looking for. Sorry. :c")
				);
			}
		});
	});
	mockedGetCard.mockImplementation((request: string) => {
		const cardName = request;
		const card = { name: cardName } as Card;
		return Promise.resolve(card);
	});
	mockedQueueHandler.mockClear();
	mockedGetCard.mockClear();
	mockedFetch.mockClear();
});

describe("getCardData", () => {
	it("should not crash without reason", async () => {
		mockedGetCard.mockImplementation((request: string) => {
			const cardName = request;
			const card = { name: cardName } as Card;
			return Promise.resolve(card);
		});
		await expect(getCardData("")).resolves.toBeTruthy();
	});
	it("should find cards in the database", async () => {
		const result = await getCardData("Liliana, Dreadhorde General");
		expect(result.name).toBe("Liliana, Dreadhorde General");
		expect(mockedFetch).toHaveBeenCalledTimes(1);
		expect(mockedGetCard).not.toHaveBeenCalled();
	});
	it("should make an api request if the card is not in the database", async () => {
		const result = await getCardData("Thief of Sanity");
		expect(result.name).toBe("Thief of Sanity");
		expect(mockedFetch).toHaveBeenCalledTimes(1);
		expect(mockedGetCard).toHaveBeenCalledTimes(1);
	});
	it("should throw an error if the search fails", async () => {
		// eslint-disable-next-line @typescript-eslint/no-unused-vars
		mockedGetCard.mockImplementation((_request: string) => {
			const resp = {
				response: {
					status: 404,
					statusText: "Not Found"
				}
			};
			return Promise.reject(resp);
		});
		async function searchNonsense(): Promise<Card> {
			try {
				const result = await getCardData("lolololol");
				return Promise.resolve(result);
			} catch (error) {
				throw new Error(error.statusText);
			}
		}
		await expect(searchNonsense()).rejects.toThrow();
	});
});
