import "@/plugins/vue-composition-api.ts";
import { mocked } from "ts-jest/utils";
import ScryfallQueueHandler from "@/toolBox/queueHandler";

// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
global.fetch = jest.fn();
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
const mockedFetch = mocked(global.fetch);

beforeEach(() => {
	mockedFetch.mockImplementation((request: string) => {
		const cardName = request.replace("https://api.scryfall.com/cards/search?q=", "");
		const resp = { data: [{ name: cardName } as Card] };
		return Promise.resolve({
			ok: true,
			statusCode: 200,
			json: () => resp
		});
	});
	mockedFetch.mockClear();
});

describe("queueHandler", () => {
	it("should not crash without reason", async () => {
		const queueHandler = new ScryfallQueueHandler();
		await expect(queueHandler.getCardByName("")).resolves.toBeTruthy();
	});
	it("should add a card to the queue when a card is looked up", async () => {
		const queueHandler = new ScryfallQueueHandler();
		const spyAddQueue = jest.spyOn(queueHandler, "addToQueue");
		const respPromise = queueHandler.getCardByName("Thief of Sanity");
		expect(queueHandler.requestQueue.length).toBe(1);
		await respPromise;
		expect(spyAddQueue).toHaveBeenCalledTimes(1);
	});
	it("should reject the promise if there is an error", async () => {
		// eslint-disable-next-line @typescript-eslint/no-unused-vars
		mockedFetch.mockImplementation((_request: string) => {
			const resp = {
				response: {
					status: 404,
					statusText: "Not Found"
				}
			};
			return Promise.reject(resp);
		});
		const queueHandler = new ScryfallQueueHandler();
		await expect(queueHandler.getCardByName("Thief of Sanity")).rejects.toBeTruthy();
	});
	it("should add as many cards to the queue as asked", async () => {
		const queueHandler = new ScryfallQueueHandler();
		const spyAddQueue = jest.spyOn(queueHandler, "addToQueue");
		const respPromiseA = queueHandler.getCardByName("Thief of Sanity");
		const respPromiseB = queueHandler.getCardByName("Thought Erasure");
		const respPromiseC = queueHandler.getCardByName("Disinformation Campaign");
		const respPromiseD = queueHandler.getCardByName("Drown in the Lock");
		expect(queueHandler.requestQueue.length).toBe(4);
		const promiseList = [respPromiseA, respPromiseB, respPromiseC, respPromiseD];
		await Promise.all(promiseList);
		expect(spyAddQueue).toHaveBeenCalledTimes(4);
	});
	it("should get the correct card data", async () => {
		const queueHandler = new ScryfallQueueHandler();
		const respPromise = queueHandler.getCardByName("Thief of Sanity");
		const response = await respPromise;
		expect(response.name).toBe("Thief of Sanity");
	});
	it("should stop looking up cards if one look up fails and rejects the rest", async () => {
		// eslint-disable-next-line @typescript-eslint/no-unused-vars
		mockedFetch.mockImplementation((_request: string) => {
			const resp = {
				response: {
					status: 404,
					statusText: "Not Found"
				}
			};
			return Promise.reject(resp);
		});
		const queueHandler = new ScryfallQueueHandler();
		await expect(queueHandler.getCardByName("Thief of Sanity")).rejects.toBeTruthy();
		await expect(queueHandler.getCardByName("Thought Erasure")).rejects.toBeTruthy();
		expect(mockedFetch).toHaveBeenCalledTimes(1);
		expect(mockedFetch).toHaveBeenLastCalledWith(
			expect.stringContaining("Thief of Sanity")
		);
	});
	it("should wait at least 20ms between each look up", async () => {
		jest.useFakeTimers();
		const mockFn = jest.fn();
		const mockedSetTimeout = (setTimeout as unknown) as typeof mockFn;
		// eslint-disable-next-line @typescript-eslint/no-unused-vars
		mockedSetTimeout.mockImplementation((func, _delay) => func());
		const queueHandler = new ScryfallQueueHandler();
		const respPromiseA = queueHandler.getCardByName("Thief of Sanity");
		const respPromiseB = queueHandler.getCardByName("Thought Erasure");
		const respPromiseC = queueHandler.getCardByName("Disinformation Campaign");
		const respPromiseD = queueHandler.getCardByName("Drown in the Lock");
		const promiseList = [respPromiseA, respPromiseB, respPromiseC, respPromiseD];
		await respPromiseA;
		expect(setTimeout).toHaveBeenCalledTimes(2);
		await respPromiseB;
		expect(setTimeout).toHaveBeenCalledTimes(3);
		await respPromiseC;
		expect(setTimeout).toHaveBeenCalledTimes(4);
		await Promise.all(promiseList);
		expect(setTimeout).toHaveBeenCalledTimes(4);
		expect(setTimeout).toHaveBeenNthCalledWith(1, expect.any(Function), 0);
		expect(setTimeout).toHaveBeenNthCalledWith(2, expect.any(Function), 20);
		expect(setTimeout).toHaveBeenNthCalledWith(3, expect.any(Function), 20);
		expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 20);
		jest.useRealTimers();
	});
});
