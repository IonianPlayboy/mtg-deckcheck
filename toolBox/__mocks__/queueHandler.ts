export const mockedGetCard = jest.fn();

const mock = jest.fn().mockImplementation(() => {
	return { getCardByName: mockedGetCard };
});
export default mock;
