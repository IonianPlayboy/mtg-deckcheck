/**
 * @jest-environment node
 */

import ScryfallQueueHandler from "@/toolBox/queueHandler";
import "@/toolBox/tsDefinitions/DecklistDefinitions";

const queueHandler = new ScryfallQueueHandler();

const getCardData = async (cardName: string): Promise<Card> => {
	const searchedCardName = cardName.split(" // ")[0];
	let error;
	const apiCard: Card = await fetch(
		`https://deckcheck-api.netlify.com/search/${searchedCardName}`
	)
		.then(res => res.json())
		.catch(err => {
			error = err;
		});
	if (!error) return apiCard;
	const scryfallCard = await queueHandler.getCardByName(cardName);
	return scryfallCard;
};

export default getCardData;
