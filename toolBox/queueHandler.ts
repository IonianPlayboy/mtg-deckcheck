/**
 * @jest-environment node
 */
import { ref, watch } from "@vue/composition-api";
import "@/toolBox/tsDefinitions/DecklistDefinitions";

// see https://github.com/gaearon/react-hot-loader/issues/94
// eslint-disable-next-line no-unused-expressions
if (process.env.NODE_ENV === "development") require("timers").setTimeout;

type promiseHandler = {
	promise: Promise<Card>;
	resolve: (value?: unknown) => void;
	reject: (value?: unknown) => void;
};

interface Ref<T> {
	value: T;
}

class ScryfallQueueHandler {
	requestQueue: Array<[string, promiseHandler]>;
	currIndex: Ref<number>;
	hasFailed: boolean;

	constructor() {
		this.requestQueue = [];
		this.currIndex = ref(0);
		this.hasFailed = false;
	}

	waitRequestTurn = <T>(value: T, resolveValue: Ref<T>): Promise<T> => {
		return new Promise(resolve => {
			watch(() => {
				if (value === resolveValue.value) resolve(value);
			});
		});
	};

	async resolvesRequest(
		currReq: number,
		requestItem: [string, promiseHandler]
	): Promise<void> {
		await this.waitRequestTurn(currReq, this.currIndex);
		if (this.hasFailed) {
			const error = new Error("A precedent request failed");
			requestItem[1].reject(error);
			return;
		}
		const delay = currReq === 0 ? 0 : 20;
		try {
			await new Promise(resolve => setTimeout(resolve, delay));
			this.currIndex.value++;
			const scryfallResponse = (await fetch(
				`https://api.scryfall.com/cards/search?q=${requestItem[0]}`
			).then(res => res.json())) as RequestResponse;
			requestItem[1].resolve(scryfallResponse.data[0]);
		} catch (error) {
			requestItem[1].reject(error);
			this.hasFailed = true;
		}
	}

	addToQueue(requestItem: [string, promiseHandler]): void {
		const currReq = this.requestQueue.push(requestItem);
		this.resolvesRequest(currReq - 1, requestItem);
	}

	getCardByName(cardName: string): Promise<Card> {
		const cardPromise = [] as Array<promiseHandler>;
		const result = new Promise(function(resolve, reject) {
			cardPromise.push({ resolve, reject } as promiseHandler);
		}) as Promise<Card>;
		cardPromise[0].promise = result;
		this.addToQueue([cardName, cardPromise[0]]);
		return result;
	}
}

export default ScryfallQueueHandler;
