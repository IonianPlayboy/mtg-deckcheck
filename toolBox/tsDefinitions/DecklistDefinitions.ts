interface Card {
	name: string;
	// eslint-disable-next-line camelcase
	tcgplayer_id: number;
	quantity: number;
}
interface RequestResponse {
	data: Array<Card>;
}

interface DeckList {
	mainboard: {
		[key: string]: Card;
	};
	sideboard: {
		[key: string]: Card;
	};
}
type boardType = keyof DeckList;

interface DeckCollection {
	[key: string]: DeckList;
}
